/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.plugins.tests;

import ch.hesso.commons.HTTPMethod;
import ch.hesso.predict.restful.APIEntity;
import ch.hesso.predict.restful.Plugin;
import ch.hesso.predict.restful.visitors.APIEntityVisitor;
import ch.hesso.websocket.annotations.*;
import ch.hesso.websocket.entities.AnnotatedPlugin;
import ch.hesso.websocket.plugins.PluginInterface;
import com.google.common.collect.ImmutableSet;

import java.util.*;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class DummyPluginInterface extends PluginInterface {

	private final Set<APIEntityVisitor> _visitors = new HashSet<> ( Arrays.<APIEntityVisitor>asList ( new DummyVisitor () ) );

	@BooleanParameter (value = true, description = "Some boolean, see !")
	private boolean someBoolean;

	@DoubleParameter (value = 10, description = "HAHAHA")
	private double someDouble;

	@ListParameter (value = { "yeah", "yo" }, description = "ROFL")
	private String[] someParameters;

	@LongParameter (value = 25, description = "MIAUH")
	private long someLong;

	@StringParameter (value = "sup", description = "niah")
	private String someString;

	@CallableMethod (value = "Call me, please", description = "It has to do something, right ?")
	public void annotatedMethod () {
		// Something has been done
		System.out.println ( someBoolean );
		System.out.println ( someDouble );
		for ( String array : someParameters ) {
			System.out.println ( array );
		}
		System.out.println ( someLong );
		System.out.println ( someString );
		PluginStatus.Builder builder = newBuilderPluginStatus ( AnnotatedPlugin.State.IDLE );
		builder.progress ( 1 );
		builder.text ( "ASDF ");
		builder.totalProgress ( 10 );
		builder.labelProgress ( "It is awesome to be here" );
		publishPluginStatus ( builder.build () );
	}

	@Override
	public String name () {
		return "dummy";
	}

	@Override
	public String description () {
		return "This is some dummy shit, you better believe it! <b>Does it support HTML?</b> I bet you it does not. BUT IT DOES !";
	}

	@Override
	public Map<Class<? extends APIEntity>, Set<HTTPMethod>> resources () {
		Map<Class<? extends APIEntity>, Set<HTTPMethod>> resources = new HashMap<> ();
		resources.put ( Plugin.class, ImmutableSet.<HTTPMethod>of ( HTTPMethod.POST, HTTPMethod.GET ) );
		return resources;
	}

	@Override
	public Set<APIEntityVisitor> visitors () {
		return _visitors;
	}

}
