/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.plugins.tests;

import ch.hesso.websocket.plugins.UnstoppableThread;
import ch.hesso.websocket.plugins.WebSocketPlugin;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class PluginInterfaceTests {

	public static void main ( final String[] args ) {

		final String server = "http://predict.es:8080/core-rest";

		UnstoppableThread unstoppable = UnstoppableThread.newThread ();
		WebSocketPlugin plugin = new WebSocketPlugin ( server, unstoppable );

		if ( !plugin.isValid () ) {
			//System.exit ( -1 );
		}

		boolean success = plugin.register ();
		if ( !success ) {
			//System.exit ( -1 );
		}

		success = plugin.listenResources ();
		if ( !success ) {
			//System.exit ( -1 );
		}

		success = plugin.startServer ();
		if ( !success ) {
			//System.exit ( -1 );
		}

		unstoppable.thread ().start ();
		try {
			while ( plugin.isShutdown () ) {
				unstoppable.thread ().join ();
			}
		} catch ( InterruptedException e ) {
		}
	}

}
