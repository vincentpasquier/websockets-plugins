/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.plugins.endpoint;

import ch.hesso.websocket.entities.AnnotatedPlugin;
import ch.hesso.websocket.entities.PluginCommunication;
import ch.hesso.websocket.plugins.PluginInterface;
import ch.hesso.websocket.plugins.PluginInterfaceProxy;
import ch.hesso.websocket.plugins.WebSocketPlugin;
import com.google.common.eventbus.Subscribe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.nio.ByteBuffer;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@ServerEndpoint (value = "/",
		encoders = {
				PluginCommunication.Coder.class,
				AnnotatedPlugin.Coder.class
		},
		decoders = {
				PluginCommunication.Coder.class,
				AnnotatedPlugin.Coder.class
		}
)
public class PluginEndpoint {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( PluginEndpoint.class );

	private Session _session;

	private PluginInterface.PluginStatus _status;

	//
	private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool ( 1 );

	private static final ConcurrentLinkedDeque<Session> _sessions = new ConcurrentLinkedDeque<> ();

	static {
		scheduler.scheduleAtFixedRate ( new PingWebSockets (), 0, 5, TimeUnit.SECONDS );
	}

	public PluginEndpoint () {
		WebSocketPlugin.Event.BUS.register ( this );
	}

	@Subscribe
	public void onPluginStatus ( final PluginInterface.PluginStatus status ) {
		_status = status;
		if ( _session != null && _session.isOpen () ) {
			_session.getAsyncRemote ().sendObject ( status.notification () );
		}
	}

	//
	public static PluginInterfaceProxy _proxy;

	@OnOpen
	public void onOpen ( final Session session, final EndpointConfig config ) {
		_session = session;
		_sessions.add ( session );
		if ( _status != null ) {
			session.getAsyncRemote ().sendObject ( _status.notification () );
		} else {
			session.getAsyncRemote ().sendObject ( _proxy.annotatedPlugin () );
		}
		session.addMessageHandler ( new MessageHandler.Whole<PluginCommunication> () {
			@Override
			public void onMessage ( final PluginCommunication message ) {
				WebSocketPlugin.Event.BUS.publish ( message );
				session.getAsyncRemote ().sendObject ( _proxy.annotatedPlugin () );
			}
		} );
	}

	private static final class PingWebSockets implements Runnable {

		private final ByteBuffer buffer = ByteBuffer.allocate ( 1 );

		public PingWebSockets () {
			buffer.put ( (byte) 0xFF );
		}

		@Override
		public void run () {
			for ( Session session : _sessions ) {
				try {
					session.getAsyncRemote ().sendPong ( buffer );
				} catch ( Exception e ) {
				}
			}
		}
	}
}
