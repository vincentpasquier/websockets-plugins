/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.plugins.endpoint;

import ch.hesso.predict.restful.APIEntity;
import ch.hesso.predict.restful.visitors.APIEntityVisitor;
import com.google.common.collect.ImmutableSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.Session;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class ClientEndpoint<T extends APIEntity> extends Endpoint {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( ClientEndpoint.class );

	//
	private final Class<T> _$class;

	//
	private final ImmutableSet<APIEntityVisitor> _visitors;

	//
	private ClientEndpoint ( final Class<T> $class, final Set<APIEntityVisitor> visitors ) {
		_$class = $class;
		_visitors = ImmutableSet.copyOf ( visitors );
	}

	/**
	 * @param $class
	 * @param visitors
	 * @param <T>
	 *
	 * @return
	 */
	public static <T extends APIEntity> ClientEndpoint<T> endpoint (
			final Class<T> $class,
			final Set<APIEntityVisitor> visitors ) {
		return new ClientEndpoint<> ( $class, visitors );
	}

	/**
	 * @param session
	 * @param config
	 */
	@Override
	public void onOpen ( final Session session, final EndpointConfig config ) {
	}
}
