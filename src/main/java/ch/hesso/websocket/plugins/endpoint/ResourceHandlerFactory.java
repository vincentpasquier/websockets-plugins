/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.plugins.endpoint;

import ch.hesso.predict.restful.*;
import ch.hesso.predict.restful.visitors.APIEntityVisitor;

import javax.websocket.MessageHandler;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public final class ResourceHandlerFactory {

	private final Set<APIEntityVisitor> _visitors;

	public ResourceHandlerFactory ( final Set<APIEntityVisitor> visitors ) {
		_visitors = visitors;
	}

	private final MessageHandler.Whole<Cfp> CFP_WHOLE = new MessageHandler.Whole<Cfp> () {
		@Override
		public void onMessage ( final Cfp message ) {
			for ( APIEntityVisitor visitor : _visitors ) {
				message.accept ( visitor );
			}
		}
	};

	private final MessageHandler.Whole<Conference> CONFERENCE_WHOLE = new MessageHandler.Whole<Conference> () {
		@Override
		public void onMessage ( final Conference message ) {
			for ( APIEntityVisitor visitor : _visitors ) {
				message.accept ( visitor );
			}
		}
	};

	private final MessageHandler.Whole<Doi> DOI_WHOLE = new MessageHandler.Whole<Doi> () {
		@Override
		public void onMessage ( final Doi message ) {
			for ( APIEntityVisitor visitor : _visitors ) {
				message.accept ( visitor );
			}
		}
	};

	private final MessageHandler.Whole<Person> PERSON_WHOLE = new MessageHandler.Whole<Person> () {
		@Override
		public void onMessage ( final Person message ) {
			for ( APIEntityVisitor visitor : _visitors ) {
				message.accept ( visitor );
			}
		}
	};

	private final MessageHandler.Whole<Plugin> PLUGIN_WHOLE = new MessageHandler.Whole<Plugin> () {
		@Override
		public void onMessage ( final Plugin message ) {
			for ( APIEntityVisitor visitor : _visitors ) {
				message.accept ( visitor );
			}
		}
	};

	private final MessageHandler.Whole<Publication> PUBLICATION_WHOLE = new MessageHandler.Whole<Publication> () {
		@Override
		public void onMessage ( final Publication message ) {
			for ( APIEntityVisitor visitor : _visitors ) {
				message.accept ( visitor );
			}
		}
	};

	private final MessageHandler.Whole<Venue> VENUE_WHOLE = new MessageHandler.Whole<Venue> () {
		@Override
		public void onMessage ( final Venue message ) {
			for ( APIEntityVisitor visitor : _visitors ) {
				message.accept ( visitor );
			}
		}
	};

	private final MessageHandler.Whole<WebPage> WEBPAGE_WHOLE = new MessageHandler.Whole<WebPage> () {
		@Override
		public void onMessage ( final WebPage message ) {
			for ( APIEntityVisitor visitor : _visitors ) {
				message.accept ( visitor );
			}
		}
	};

	public MessageHandler handler ( final Class<? extends APIEntity> $class ) {
		if ( Cfp.class.equals ( $class ) ) {
			return CFP_WHOLE;
		} else if ( Conference.class.equals ( $class ) ) {
			return CONFERENCE_WHOLE;
		} else if ( Doi.class.equals ( $class ) ) {
			return DOI_WHOLE;
		} else if ( Person.class.equals ( $class ) ) {
			return PERSON_WHOLE;
		} else if ( Plugin.class.equals ( $class ) ) {
			return PLUGIN_WHOLE;
		} else if ( Publication.class.equals ( $class ) ) {
			return PUBLICATION_WHOLE;
		} else if ( Venue.class.equals ( $class ) ) {
			return VENUE_WHOLE;
		} else if ( WebPage.class.equals ( $class ) ) {
			return WEBPAGE_WHOLE;
		}
		return null;
	}
}
