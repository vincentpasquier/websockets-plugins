/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.plugins;

import ch.hesso.predict.client.APIClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public final class WebSocketClientMain {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( WebSocketClientMain.class );

	public static void main ( final String[] args ) {

		// 1) Read PluginInterface sub
		//	1.1) Instanciate
		//	1.2) Get name, and so on, create Plugin
		//	1.3) Generate shit
		// 2) Send to server: register plugin
		// 3) Get response, get all WS -> listen
		// 4) Create own WS
		// 5) Listen to own WS

		final String server = APIClient.REST_API;

		final UnstoppableThread unstoppable = UnstoppableThread.newThread ();
		final WebSocketPlugin plugin = new WebSocketPlugin ( server, unstoppable );

		if ( !plugin.isValid () ) {
			LOG.error ( "No valid plugin available, please check logs." );
			System.exit ( -1 );
		}

		boolean success = plugin.register ();
		if ( !success ) {
			LOG.error ( "Plugin registration failed, please check logs." );
			System.exit ( -1 );
		}

		success = plugin.listenResources ();
		if ( !success ) {
			LOG.error ( "Plugin failed to connect to resources, please check logs." );
			System.exit ( -1 );
		}

		success = plugin.startServer ();
		if ( !success ) {
			LOG.error ( "Plugin server start failed, please check logs." );
			System.exit ( -1 );
		}

		final Thread mainThread = Thread.currentThread ();
		Runtime.getRuntime ().addShutdownHook ( new Thread () {
			public void run () {
				try {
					plugin.stop ();
					unstoppable.stop ();
					mainThread.join ();
				} catch ( Exception e ) {
				}
			}
		} );

		unstoppable.thread ().start ();
		try {
			while ( plugin.isShutdown () ) {
				unstoppable.thread ().join ();
			}
		} catch ( InterruptedException e ) {
			System.out.println ( e );
		}

	}


}
