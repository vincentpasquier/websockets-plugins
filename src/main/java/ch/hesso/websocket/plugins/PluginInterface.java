/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.plugins;

import ch.hesso.commons.HTTPMethod;
import ch.hesso.predict.restful.APIEntity;
import ch.hesso.predict.restful.visitors.APIEntityVisitor;
import ch.hesso.websocket.entities.AnnotatedPlugin;

import java.util.Map;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public abstract class PluginInterface {

	public PluginInterface () {
	}

	public abstract String name ();

	public abstract String description ();

	public abstract Map<Class<? extends APIEntity>, Set<HTTPMethod>> resources ();

	public abstract Set<APIEntityVisitor> visitors ();

	public static final PluginStatus.Builder newBuilderPluginStatus ( final AnnotatedPlugin.State state ) {
		return new PluginStatus.Builder ( state );
	}

	public static final void publishPluginStatus ( final PluginStatus status ) {
		WebSocketPlugin.Event.BUS.publish ( status );
	}

	public static final class PluginStatus {

		private final AnnotatedPlugin _annotatedPlugin;

		private PluginStatus ( final Builder builder ) {
			_annotatedPlugin = builder._annotatedPlugin;
		}

		public AnnotatedPlugin notification () {
			return _annotatedPlugin;
		}

		public static final class Builder {

			private final AnnotatedPlugin _annotatedPlugin;

			private Builder ( final AnnotatedPlugin.State state ) {
				_annotatedPlugin = new AnnotatedPlugin ();
				_annotatedPlugin.setState ( state );
			}

			public Builder status ( final AnnotatedPlugin.State status ) {
				_annotatedPlugin.setState ( status );
				return this;
			}

			public Builder text ( final String text ) {
				_annotatedPlugin.setText ( text );
				return this;
			}

			public Builder totalProgress ( final int totalProgress ) {
				_annotatedPlugin.setTotalProgress ( totalProgress );
				return this;
			}

			public Builder progress ( final int progress ) {
				_annotatedPlugin.setProgress ( progress );
				return this;
			}

			public Builder totalSubProgress ( final int totalSubProgress ) {
				_annotatedPlugin.setTotalSubProgress ( totalSubProgress );
				return this;
			}

			public Builder subProgress ( final int subProgress ) {
				_annotatedPlugin.setSubProgress ( subProgress );
				return this;
			}

			public Builder labelProgress ( final String label ) {
				_annotatedPlugin.setLabelProgress ( label );
				return this;
			}

			public Builder labelSubProgress ( final String subLabel ) {
				_annotatedPlugin.setLabelSubProgress ( subLabel );
				return this;
			}

			public Builder objects ( final Map<String, Object> objects ) {
				_annotatedPlugin.setObjects ( objects );
				return this;
			}

			public PluginStatus build () {
				return new PluginStatus ( this );
			}
		}
	}

}
