/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.plugins;

import ch.hesso.reflect.AnnotatedMemberFinder;
import ch.hesso.websocket.annotations.*;
import ch.hesso.websocket.entities.AnnotatedPlugin;
import ch.hesso.websocket.entities.PluginCommunication;
import com.google.common.base.Joiner;
import com.google.common.eventbus.Subscribe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class PluginInterfaceProxy {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( PluginInterfaceProxy.class );

	private final PluginInterface _pi;

	private final Class<? extends PluginInterface> _$class;

	private final Set<PluginProxy> _proxies;

	public PluginInterfaceProxy ( final PluginInterface pi, final Class<? extends PluginInterface> $class ) {
		_pi = pi;
		_$class = $class;
		_proxies = new HashSet<> ();
		WebSocketPlugin.Event.BUS.register ( this );
		buildMethods ();
		buildBooleans ();
		buildDoubles ();
		buildLists ();
		buildLongs ();
		buildStrings ();
	}

	@Subscribe
	public void onCommunication ( final PluginCommunication communication ) {
		for ( PluginProxy proxy : _proxies ) {
			proxy.update ( communication );
		}
		String methodName = communication.getExecute ();
		try {
			final Method method = _$class.getDeclaredMethod ( methodName );
			//method.invoke ( _pi );
			Runnable run = new Runnable () {
				@Override
				public void run () {
					try {
						method.invoke ( _pi );
					} catch ( IllegalAccessException | InvocationTargetException e ) {
						LOG.debug ( "Error while invoking method", e );
					}
				}
			};
			new Thread ( run ).start ();
		} catch ( NoSuchMethodException e ) {
			LOG.error ( String.format ( "Error while calling [%s], check logs", methodName ), e );
		}
		WebSocketPlugin.Event.BUS.publish ( annotatedPlugin () );
	}

	public AnnotatedPlugin annotatedPlugin () {
		AnnotatedPlugin plugin = new AnnotatedPlugin ();
		for ( PluginProxy proxy : _proxies ) {
			proxy.annotatedPlugin ( plugin );
		}
		return plugin;
	}

	private void buildMethods () {
		Map<Method, CallableMethod> methods =
				AnnotatedMemberFinder.findAnnotatedMethod ( _$class, CallableMethod.class );
		for ( Map.Entry<Method, CallableMethod> method : methods.entrySet () ) {
			CallableMethod parameter = method.getKey ().getAnnotation ( CallableMethod.class );
			MethodProxy proxy = new MethodProxy ( method.getKey (), parameter );
			_proxies.add ( proxy );
		}
	}

	private void buildBooleans () {
		Map<Field, BooleanParameter> booleans =
				AnnotatedMemberFinder.findAnnotatedMember ( _$class, BooleanParameter.class );
		for ( Map.Entry<Field, BooleanParameter> field : booleans.entrySet () ) {
			BooleanParameter parameter = field.getKey ().getAnnotation ( BooleanParameter.class );
			BooleanProxy proxy = new BooleanProxy ( _pi, field.getKey (), parameter );
			_proxies.add ( proxy );
		}
	}

	private void buildDoubles () {
		Map<Field, DoubleParameter> doubles =
				AnnotatedMemberFinder.findAnnotatedMember ( _$class, DoubleParameter.class );
		for ( Map.Entry<Field, DoubleParameter> field : doubles.entrySet () ) {
			DoubleParameter parameter = field.getKey ().getAnnotation ( DoubleParameter.class );
			DoubleProxy proxy = new DoubleProxy ( _pi, field.getKey (), parameter );
			_proxies.add ( proxy );
		}
	}

	private void buildLists () {
		Map<Field, ListParameter> lists =
				AnnotatedMemberFinder.findAnnotatedMember ( _$class, ListParameter.class );
		for ( Map.Entry<Field, ListParameter> field : lists.entrySet () ) {
			ListParameter parameter = field.getKey ().getAnnotation ( ListParameter.class );
			ListProxy proxy = new ListProxy ( _pi, field.getKey (), parameter );
			_proxies.add ( proxy );
		}
	}

	private void buildLongs () {
		Map<Field, LongParameter> longs =
				AnnotatedMemberFinder.findAnnotatedMember ( _$class, LongParameter.class );
		for ( Map.Entry<Field, LongParameter> field : longs.entrySet () ) {
			LongParameter parameter = field.getKey ().getAnnotation ( LongParameter.class );
			LongProxy proxy = new LongProxy ( _pi, field.getKey (), parameter );
			_proxies.add ( proxy );
		}
	}

	private void buildStrings () {
		Map<Field, StringParameter> strings =
				AnnotatedMemberFinder.findAnnotatedMember ( _$class, StringParameter.class );
		for ( Map.Entry<Field, StringParameter> field : strings.entrySet () ) {
			StringParameter parameter = field.getKey ().getAnnotation ( StringParameter.class );
			StringProxy proxy = new StringProxy ( _pi, field.getKey (), parameter );
			_proxies.add ( proxy );
		}
	}

	public static interface PluginProxy {

		void update ( final PluginCommunication communication );

		void annotatedPlugin ( final AnnotatedPlugin plugin );

	}

	public static final class MethodProxy implements PluginProxy {

		private final Method _method;

		private final CallableMethod _parameter;

		public MethodProxy ( final Method method, final CallableMethod parameter ) {
			_method = method;
			_parameter = parameter;
		}

		@Override
		public void update ( final PluginCommunication communication ) {
			// Nothing
		}

		@Override
		public void annotatedPlugin ( final AnnotatedPlugin plugin ) {
			Map<String, String> parameters = new HashMap<> ();
			parameters.put ( "description", _parameter.description () );
			parameters.put ( "label", _parameter.value () );
			parameters.put ( "name", _method.getName () );
			plugin.addMethods ( _method.getName (), parameters );
		}
	}


	public static final class BooleanProxy implements PluginProxy {

		private final Object _object;

		private final Field _field;

		private final BooleanParameter _parameter;

		public BooleanProxy ( final Object object, final Field field, final BooleanParameter parameter ) {
			_object = object;
			_field = field;
			_parameter = parameter;
			set ( _parameter.value () );
		}

		@Override
		public void update ( final PluginCommunication communication ) {
			boolean contains = communication.getBooleans ().containsKey ( _field.getName () );
			if ( contains ) {
				boolean value = communication.getBooleans ().get ( _field.getName () );
				set ( value );
			}
		}

		@Override
		public void annotatedPlugin ( final AnnotatedPlugin plugin ) {
			boolean value = false;
			try {
				value = _field.getBoolean ( _object );
			} catch ( IllegalAccessException e ) {
				LOG.debug ( String.format ( "Unable to access boolean parameter %s", _field.getName () ), e );
			}
			Map<String, String> parameters = new HashMap<> ();
			parameters.put ( "value", String.valueOf ( value ) );
			parameters.put ( "description", _parameter.description () );
			parameters.put ( "type", "boolean" );
			parameters.put ( "name", _field.getName () );
			plugin.addBooleans ( _field.getName (), parameters );
		}

		public void set ( boolean value ) {
			try {
				_field.setBoolean ( _object, value );
			} catch ( IllegalAccessException e ) {
				LOG.debug ( String.format ( "Unable to access boolean parameter %s", _field.getName () ), e );
			}
		}
	}

	public static final class DoubleProxy implements PluginProxy {

		private final Object _object;

		private final Field _field;

		private final DoubleParameter _parameter;

		public DoubleProxy ( final Object object, final Field field, final DoubleParameter parameter ) {
			_object = object;
			_field = field;
			_parameter = parameter;
			set ( _parameter.value () );
		}

		@Override
		public void update ( final PluginCommunication communication ) {
			boolean contains = communication.getDoubles ().containsKey ( _field.getName () );
			if ( contains ) {
				double value = communication.getDoubles ().get ( _field.getName () );
				set ( value );
			}
		}

		@Override
		public void annotatedPlugin ( final AnnotatedPlugin plugin ) {
			double value = 0.0;
			try {
				value = _field.getDouble ( _object );
			} catch ( IllegalAccessException e ) {
				LOG.debug ( String.format ( "Unable to access double parameter %s", _field.getName () ), e );
			}
			Map<String, String> parameters = new HashMap<> ();
			parameters.put ( "value", String.valueOf ( value ) );
			parameters.put ( "description", _parameter.description () );
			parameters.put ( "min", String.valueOf ( _parameter.min () ) );
			parameters.put ( "max", String.valueOf ( _parameter.max () ) );
			parameters.put ( "type", "double" );
			parameters.put ( "name", _field.getName () );
			plugin.addDoubles ( _field.getName (), parameters );
		}

		public void set ( double value ) {
			if ( _parameter.max () < _parameter.min () ) {
				value = Math.max ( value, _parameter.min () );
				value = Math.min ( value, _parameter.max () );
			}
			try {
				_field.setDouble ( _object, value );
			} catch ( IllegalAccessException e ) {
				LOG.debug ( String.format ( "Unable to access double parameter %s", _field.getName () ), e );
			}
		}
	}

	public static final class ListProxy implements PluginProxy {

		private final Object _object;

		private final Field _field;

		private final ListParameter _parameter;

		public ListProxy ( final Object object, final Field field, final ListParameter parameter ) {
			_object = object;
			_field = field;
			_parameter = parameter;
			set ( _parameter.value () );
		}

		@Override
		public void update ( final PluginCommunication communication ) {
			boolean contains = communication.getLists ().containsKey ( _field.getName () );
			if ( contains ) {
				String[] value = communication.getLists ().get ( _field.getName () );
				set ( value );
			}
		}

		@Override
		public void annotatedPlugin ( final AnnotatedPlugin plugin ) {
			String[] values = new String[] { };
			try {
				values = (String[]) _field.get ( _object );
			} catch ( IllegalAccessException e ) {
				LOG.debug ( String.format ( "Unable to access string array parameter %s", _field.getName () ), e );
			} catch ( ClassCastException e ) {
				LOG.debug ( String.format ( "Parameter [%s] is not a String array", _field.getName () ), e );
			}
			Map<String, String> parameters = new HashMap<> ();
			String joined = Joiner.on ( "," ).join ( values );
			parameters.put ( "value", joined );
			parameters.put ( "size", String.valueOf ( values.length ) );
			parameters.put ( "description", _parameter.description () );
			parameters.put ( "type", "array" );
			parameters.put ( "name", _field.getName () );
			plugin.addLists ( _field.getName (), parameters );
		}

		public void set ( final String[] value ) {
			try {
				_field.set ( _object, value );
			} catch ( IllegalAccessException e ) {
				LOG.debug ( String.format ( "Unable to access array parameter %s", _field.getName () ), e );
			}
		}
	}

	public static final class LongProxy implements PluginProxy {

		private final Object _object;

		private final Field _field;

		private final LongParameter _parameter;

		public LongProxy ( final Object object, final Field field, final LongParameter parameter ) {
			_object = object;
			_field = field;
			_parameter = parameter;
			set ( _parameter.value () );
		}

		@Override
		public void update ( final PluginCommunication communication ) {
			boolean contains = communication.getLongs ().containsKey ( _field.getName () );
			if ( contains ) {
				long value = communication.getLongs ().get ( _field.getName () );
				set ( value );
			}
		}

		@Override
		public void annotatedPlugin ( final AnnotatedPlugin plugin ) {
			long value = 0;
			try {
				value = _field.getLong ( _object );
			} catch ( IllegalAccessException e ) {
				LOG.debug ( String.format ( "Unable to access long parameter %s", _field.getName () ), e );
			}
			Map<String, String> parameters = new HashMap<> ();
			parameters.put ( "value", String.valueOf ( value ) );
			parameters.put ( "description", _parameter.description () );
			parameters.put ( "min", String.valueOf ( _parameter.min () ) );
			parameters.put ( "max", String.valueOf ( _parameter.max () ) );
			parameters.put ( "type", "long" );
			parameters.put ( "name", _field.getName () );
			plugin.addLongs ( _field.getName (), parameters );
		}

		public void set ( long value ) {
			if ( _parameter.max () < _parameter.min () ) {
				value = Math.max ( value, _parameter.min () );
				value = Math.min ( value, _parameter.max () );
			}
			try {
				_field.setLong ( _object, value );
			} catch ( IllegalAccessException e ) {
				LOG.debug ( String.format ( "Unable to access long parameter %s", _field.getName () ), e );
			}
		}
	}

	public static final class StringProxy implements PluginProxy {

		private final Object _object;

		private final Field _field;

		private final StringParameter _parameter;

		public StringProxy ( final Object object, final Field field, final StringParameter parameter ) {
			_object = object;
			_field = field;
			_parameter = parameter;
			set ( _parameter.value () );
		}

		@Override
		public void update ( final PluginCommunication communication ) {
			boolean contains = communication.getStrings ().containsKey ( _field.getName () );
			if ( contains ) {
				String value = communication.getStrings ().get ( _field.getName () );
				set ( value );
			}
		}

		@Override
		public void annotatedPlugin ( final AnnotatedPlugin plugin ) {
			String value = "";
			try {
				value = (String) _field.get ( _object );
			} catch ( IllegalAccessException e ) {
				LOG.debug ( String.format ( "Unable to access string array parameter %s", _field.getName () ), e );
			} catch ( ClassCastException e ) {
				LOG.debug ( String.format ( "Parameter [%s] is not a String", _field.getName () ), e );
			}
			Map<String, String> parameters = new HashMap<> ();
			parameters.put ( "value", value );
			parameters.put ( "description", _parameter.description () );
			parameters.put ( "type", "string" );
			parameters.put ( "name", _field.getName () );
			plugin.addStrings ( _field.getName (), parameters );
		}

		public void set ( String value ) {
			try {
				_field.set ( _object, value );
			} catch ( IllegalAccessException e ) {
				LOG.debug ( String.format ( "Unable to access string parameter %s", _field.getName () ), e );
			}
		}
	}
}
