/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.websocket.plugins;

import ch.hesso.commons.HTTPMethod;
import ch.hesso.predict.client.resources.PluginClient;
import ch.hesso.predict.restful.APIEntity;
import ch.hesso.predict.restful.Plugin;
import ch.hesso.reflect.TopLevelClassFinder;
import ch.hesso.websocket.entities.JSONCoder;
import ch.hesso.websocket.plugins.endpoint.ClientEndpoint;
import ch.hesso.websocket.plugins.endpoint.PluginEndpoint;
import ch.hesso.websocket.plugins.endpoint.ResourceHandlerFactory;
import ch.hesso.websocket.server.endpoints.CoderGenerator;
import com.google.common.eventbus.EventBus;
import org.glassfish.tyrus.client.ClientManager;
import org.glassfish.tyrus.server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.util.logging.resources.logging;

import javax.websocket.*;
import javax.ws.rs.WebApplicationException;
import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public final class WebSocketPlugin {

	//
	private static final String HOST_IP;

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( WebSocketPlugin.class );

	static {
		String ip = System.getProperty ( "websockets.host.ip" );
		if ( ip == null ) {
			LOG.info ( "Host IP not set, using default (localhost). To set it use websockets.host.ip" );
			ip = "localhost";
		}
		HOST_IP = ip;
	}

	//
	private final UnstoppableThread _unstoppable;

	//
	private final String _serverURL;

	//
	private final CoderGenerator<JSONCoder> _coderGenerator;

	//
	private final Map<String, ClientEndpoint> _endpoints;

	//
	private final Map<String, Class<? extends Encoder.TextStream>> _coders;

	//
	private final Map<String, Class<? extends Decoder.TextStream>> _decoders;

	//
	private final Map<String, Session> _sessions;

	//
	private final Map<String, MessageHandler> _handlers;

	//
	private Class<? extends PluginInterface> _$class;

	//
	private Plugin response;

	//
	private Server _server;

	//
	private PluginInterface _pi;

	//
	private PluginInterfaceProxy _proxy;

	/**
	 * @param unstoppable
	 */
	public WebSocketPlugin ( final String serverURL, final UnstoppableThread unstoppable ) {
		_serverURL = serverURL;
		_unstoppable = unstoppable;
		_coderGenerator = new CoderGenerator<> ( JSONCoder.class );
		_endpoints = new HashMap<> ();
		_coders = new HashMap<> ();
		_decoders = new HashMap<> ();
		_sessions = new HashMap<> ();
		_handlers = new HashMap<> ();
		TopLevelClassFinder<PluginInterface> finder = new TopLevelClassFinder<> ();
		Set<Class<? extends PluginInterface>> pis = finder.findInstantiableSubClasses ( PluginInterface.class, "ch" );

		if ( pis.size () == 1 ) {
			final Class<? extends PluginInterface> $class = pis.iterator ().next ();
			try {
				_$class = $class;
				_pi = $class.newInstance ();
				_proxy = new PluginInterfaceProxy ( _pi, _$class );
			} catch ( InstantiationException | IllegalAccessException e ) {
				LOG.error ( "Error while instancing PluginInstance", e );
			}
		} else {
			LOG.debug ( String.format ( "Number of plugin found incorrect (should be 1): %d", pis.size () ) );
			StringBuilder sb = new StringBuilder ( "Found:\n" );
			for ( Class<? extends PluginInterface> pi : pis ) {
				sb.append ( pi.getCanonicalName () ).append ( "\n" );
			}
			LOG.debug ( sb.toString () );
		}
	}

	/**
	 * @return
	 */
	public boolean isValid () {
		return _pi != null;
	}

	/**
	 * @return
	 */
	public boolean isShutdown () {
		return true;
	}

	/**
	 * @return
	 */
	public boolean register () {
		boolean success = true;

		try {
			Plugin plugin = buildPlugin ();
			PluginClient client = PluginClient.create ( _serverURL );
			response = client.createPlugin ( plugin );
		} catch ( InstantiationException | IllegalAccessException e ) {
			LOG.debug ( String.format ( "Error while creating plugin [%s]", _pi.name () ), e );
			success = false;
		} catch ( WebApplicationException e ) {
			LOG.debug ( String.format ( "Error while registering to server [%s]", _serverURL ), e );
			success = false;
		} catch ( Exception e ) {
			LOG.debug ( String.format ( "Error while creating endpoints coders/decoders to server [%s]", _serverURL ), e );
			success = false;
		}

		return success;
	}

	/**
	 * @return
	 *
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public Plugin buildPlugin () throws Exception {
		Plugin plugin = new Plugin ();
		plugin.setName ( _pi.name () );
		plugin.setDescription ( _pi.description () );
		plugin.setWebSocket ( "ws://" + HOST_IP ); // TODO add support for more
		try {
			plugin.setRegistrations ( generateResources () );
		} catch ( IllegalAccessException | InstantiationException e ) {
			LOG.debug ( "Error while generating resource query", e );
			throw e;
		} catch ( Exception e ) {
			LOG.debug ( "Generating coder/decoders failed, check logs", e );
			throw e;
		}
		return plugin;
	}

	//
	@SuppressWarnings ("unchecked")
	private Map<String, Set<HTTPMethod>> generateResources () throws Exception {
		Map<String, Set<HTTPMethod>> resources = new HashMap<> ();
		ResourceHandlerFactory factory = new ResourceHandlerFactory ( _pi.visitors () );
		for ( Map.Entry<Class<? extends APIEntity>, Set<HTTPMethod>> res : _pi.resources ().entrySet () ) {
			Class<? extends APIEntity> to = res.getKey ();
			String resourceName = to.newInstance ().resourceName ();
			resources.put ( resourceName, res.getValue () );
			Class<? extends Encoder.TextStream> coder = _coderGenerator.makeCoder ( to );
			_coders.put ( resourceName, coder );
			// Unchecked: makeCoder always returns a class that extends both Encoder and Decoder TextStream
			Class<? extends Decoder.TextStream> decoder = (Class<? extends Decoder.TextStream>) coder;
			_decoders.put ( resourceName, decoder );
			ClientEndpoint<? extends APIEntity> endpoint = ClientEndpoint.endpoint ( to, _pi.visitors () );
			_endpoints.put ( resourceName, endpoint );
			_handlers.put ( resourceName, factory.handler ( res.getKey () ) );
		}
		return resources;
	}

	/**
	 * @return
	 */
	public boolean startServer () {
		boolean success = true;
		try {
			String[] hostPort = response.getWebSocket ().split ( ":" );
			int port = Integer.parseInt ( hostPort[ hostPort.length - 1 ] );
			// TODO Check to update localhost
			_server = new Server ( HOST_IP, port, "/", null, PluginEndpoint.class );
			PluginEndpoint._proxy = _proxy;
			_server.start ();
		} catch ( NumberFormatException e ) {
			LOG.debug ( String.format ( "Error while starting server, wrong parameters received [%s]", _serverURL ), e );
			success = false;
		} catch ( DeploymentException e ) {
			LOG.debug ( "Error while starting server, check logs.", e );
			success = false;
		}
		return success;
	}

	/**
	 * @return
	 */
	public boolean listenResources () {
		boolean success = true;
		ClientManager client = ClientManager.createClient ();
		try {
			for ( Map.Entry<String, String> entry : response.getWebSockets ().entrySet () ) {
				String registration = entry.getKey ();
				if ( _endpoints.containsKey ( registration ) ) {
					ClientEndpoint endpoint = _endpoints.get ( registration );
					Class<? extends Encoder.TextStream> encoder = _coders.get ( registration );
					Class<? extends Decoder.TextStream> decoder = _decoders.get ( registration );
					Session session = client.connectToServer ( endpoint,
							ClientEndpointConfig.Builder.create ()
									.encoders ( Arrays.<Class<? extends Encoder>>asList ( encoder ) )
									.decoders ( Arrays.<Class<? extends Decoder>>asList ( decoder ) )
									.build (),
							URI.create ( entry.getValue () ) );
					MessageHandler handler = _handlers.get ( registration );
					session.addMessageHandler ( handler );
					_sessions.put ( registration, session );
				} else {
					LOG.debug ( String.format ( "Unable to find endpoint for [%s], check logs.", registration ) );
					success = false;
				}
			}
		} catch ( DeploymentException | IOException e ) {
			LOG.error ( "Error while listening to resources, check logs.", e );
			success = false;
		}
		return success;
	}

	public void stop () {
		PluginClient client = PluginClient.create ( _serverURL );
		client.delete ( response );
	}

	public static enum Event {
		BUS;

		private final EventBus _bus;

		private Event () {
			_bus = new EventBus ();
		}

		public void register ( final Object object ) {
			_bus.register ( object );
		}

		public void publish ( final Object message ) {
			_bus.post ( message );
		}
	}
}
